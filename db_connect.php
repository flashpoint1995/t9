<?php

class Cantacts {
    const SERVER_NAME = "localhost";
    const USERNAME = "root";
    const PASSWORD = "";
    const DB_NAME = "t9";

    const TABLE_NAME = "contacts";
    
    /**
     * internal connection method
     *
     * @return mixed
     */
    private function connect() {
        // Create connection
        $conn = new mysqli(self::SERVER_NAME, self::USERNAME, self::PASSWORD, self::DB_NAME);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }
    
    /**
     * itnernal disconnect method
     *
     * @param  mixed $conn
     * @return void
     */
    private function disconnect($conn) {
        $conn->close();
    }
    
    /**
     * Add new contact to DB
     *
     * @param  mixed $firstname
     * @param  mixed $lastname
     * @param  mixed $phone
     * @return void
     */
    public function insertContact($firstname, $lastname, $phone) {
        $conn = $this->connect();

        $sql = "INSERT INTO " . self::TABLE_NAME . " (firstname, lastname, phone)
        VALUES ('$firstname', '$lastname', '$phone')";

        if ($conn->query($sql) !== TRUE) {
            die("Error: " . $sql . "<br>" . $conn->error);
        }

        $this->disconnect($conn);
    }
    
    /**
     * Search contacts based on numbers of old mobile phones
     *
     * @param  mixed $search
     * @return array
     */
    public function searchContacts($search) {
        if (empty($search)) {
            return [];
        }
        
        $conn = $this->connect();

        // map of an old mobile phone and its chars
        $map = [
            " ", #0
            NULL, "abc", "def",
            "ghi", "jkl", "mno",
            "pqrs", "tuv", "wxyz"
        ];

        $condition = "^";
        foreach (str_split("$search") as $digit) {
            if ($digit == 1) {
                return []; # no result because no char exists on this digit
            }

            $condition .= "[$map[$digit]]";
        }

        $sql = "SELECT * FROM " . self::TABLE_NAME . " WHERE CONCAT(firstname, \" \", lastname) REGEXP '$condition' || CONCAT(lastname, \" \", firstname) REGEXP '$condition'";
        $queryResult = $conn->query($sql);

        $result = [];
        if (!empty($queryResult->num_rows)) {
            // save row as result
            while($row = $queryResult->fetch_assoc()) {
                $result[] = $row;
            }
        }

        $this->disconnect($conn);

        return $result;
    }
}

?>