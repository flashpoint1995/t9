<?php if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    include "db_connect.php";
    $contacts = new Cantacts();

    // if it is a numeric string
    if (ctype_digit($_POST["search"])) {
        $result = $contacts->searchContacts($_POST["search"]);
    }
}   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="insert_contact.php">Kontakt anlegen</a>
    <hr><br>
    <form method="POST" action="index.php">
        <input type="search" placeholder="688" name="search" id="search" value="<?=!empty($_POST['search']) ? $_POST['search'] : ''?>">
        <button type="submit">Kontakt suchen</button>
    </form>
    <br>

    <?php if (!empty($result)): ?>
        <table>
            <thead>
                <tr>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Telefonnummer</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($result as $contact) : ?>
                    <tr>
                        <td>
                            <?=$contact['firstname']?>
                        </td>
                        <td>
                            <?=$contact['lastname']?>
                        </td>
                        <td>
                            <?=$contact['phone']?>
                        </td>
                    </tr>
                <?php endforeach?>
            </tbody>
        </table>
    <?php endif ?>

    <?php if ($_SERVER['REQUEST_METHOD'] === 'POST' && empty($result)): ?>
        Kein Ergebniss gefunden
    <?php endif ?>
</body>
</html>