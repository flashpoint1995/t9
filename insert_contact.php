<?php if ($_SERVER['REQUEST_METHOD'] === 'GET'): ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <a href="index.php">Zurück zum Anfang</a>
        <hr>
        <form method="POST">
            <label for="firstname">Vorname</label>
            <input type="text" name="firstname" id="firstname">
            <br>

            <label for="lastname">Nachname</label>
            <input type="text" name="lastname" id="lastname">
            <br>

            <label for="phone">Telefonnummer</label>
            <input type="tel" name="phone" id="phone">
            <br>
            <br>

            <button type="submit">Kontakt anlegen</button>
        </form>
    </body>
    </html>
<?php endif ?>

<?php if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['phone'])) {
        echo 'Alle Felder sind Pflichtfelder. <a href="index.php">Zurück zum Anfang</a>';
        return;
    }

    if (preg_match('~[0-9]+~', $_POST['firstname']) || preg_match('~[0-9]+~', $_POST['lastname'])) {
        echo 'Vorname und Nachname dürfen keine Ziffern enthalten. <a href="index.php">Zurück zum Anfang</a>';
        return;
    }

    include "db_connect.php";

    $contacts = new Cantacts();
    $contacts->insertContact($_POST['firstname'], $_POST['lastname'], $_POST['phone']);

    echo 'Kontakt wurde angelegt. <a href="index.php">Zurück zum Anfang</a>';
} 
?>